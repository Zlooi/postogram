import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import '../bloc/auth/auth_bloc.dart';
import 'home/discover/discover_page.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => RegisterPageState();

}

class RegisterPageState extends State<StatefulWidget> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocConsumer<AuthBloc, AuthState>(
          listener: (context, state) {
            if (state is Authenticated) {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => DiscoverPage())
              );
            }
            else if(state is AuthError) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text(state.error))
              );
            }
          },
          builder: (context, state) {
              return Padding(
                padding: EdgeInsets.all(16.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(
                        height: 16.0,
                      ),
                      Align(
                          alignment: Alignment.topLeft,
                          child: SizedBox(
                            width: 48.0,
                            height: 48.0,
                            child: InkWell(
                              child: Icon(Icons.arrow_back),
                              onTap: () {
                                Navigator.pop(context);
                              },
                              radius: 32.0,
                              borderRadius: BorderRadius.circular(32.0),
                            ),
                          )
                      ),
                      Text(
                        'Register',
                        style: GoogleFonts.comfortaa(
                          color: Colors.black,
                          fontStyle: FontStyle.normal,
                          fontSize: 36,
                          letterSpacing: -1.5,
                        ),
                      ),
                      SizedBox(height: 32.0),

                      TextFormField(
                        decoration: InputDecoration(
                          hintText: 'E-mail',
                          errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),

                        ),
                        keyboardType: TextInputType.emailAddress,
                        controller: _emailController,
                        autovalidateMode:
                        AutovalidateMode.onUserInteraction,
                        validator: (value) {
                          return value != null &&
                              !EmailValidator.validate(value)
                              ? 'Enter a valid email'
                              : null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Password',
                          errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                        ),
                        controller: _passwordController,
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                        autovalidateMode:
                        AutovalidateMode.onUserInteraction,
                        validator: (value) {
                          return value != null && value.length < 6
                              ? "Enter min. 6 characters"
                              : null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      SizedBox(
                        height: 52,
                        child: TextButton(
                          onPressed: () {
                            _createAccountWithEmailAndPassword(context);
                          },
                          child: Text(
                            'NEXT',
                            style: GoogleFonts.roboto(
                                color: Colors.white
                            ),
                          ),
                          style: ButtonStyle(
                              elevation: MaterialStateProperty.all(1.0),
                              backgroundColor: MaterialStateProperty.all(Colors.black),
                              shadowColor: MaterialStateProperty.all(Colors.grey),
                              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0)
                              ))
                          ),
                        ),
                      ),

                      Spacer(),
                      if(state is Loading)
                        Center(
                          child: CircularProgressIndicator(),
                        )
                    ],
                  ),
                )
              );

          }
        )
    );
  }

  void _createAccountWithEmailAndPassword(context) {
    if (_formKey.currentState!.validate()) {
      // If email is valid adding new event [SignUpRequested].
      BlocProvider.of<AuthBloc>(context).add(
        RegisterRequested(_emailController.text, _passwordController.text),
      );
    }
  }
}

class _RegisterPageTwo extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Material(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                  alignment: Alignment.topLeft,
                  child: SizedBox(
                    width: 48.0,
                    height: 48.0,
                    child: InkWell(
                      child: Icon(Icons.arrow_back),
                      onTap: () { Navigator.pop(context); },
                      radius: 32.0,
                      borderRadius: BorderRadius.circular(32.0),
                    ),
                  )
              ),
              Text(
                'Register',
                style: GoogleFonts.comfortaa(
                  color: Colors.black,
                  fontStyle: FontStyle.normal,
                  fontSize: 36,
                  letterSpacing: -1.5,
                ),
              ),
              SizedBox(height: 32.0),
              const TextField(
                decoration: InputDecoration(
                  hintText: 'Nickname',
                  errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 3.0),
                      borderRadius: BorderRadius.zero
                  ),
                  disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 3.0),
                      borderRadius: BorderRadius.zero
                  ),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(width: 3.0),
                      borderRadius: BorderRadius.zero
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 3.0),
                      borderRadius: BorderRadius.zero
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 3.0),
                      borderRadius: BorderRadius.zero
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 3.0),
                      borderRadius: BorderRadius.zero
                  ),

                ),
                keyboardType: TextInputType.name,
              ),
              const SizedBox(height: 16.0),
              SizedBox(
                height: 52,
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DiscoverPage()
                      )
                    );
                  },
                  child: Text(
                    'SIGN UP',
                    style: GoogleFonts.roboto(
                        color: Colors.white
                    ),
                  ),
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all(1.0),
                      backgroundColor: MaterialStateProperty.all(Colors.black),
                      shadowColor: MaterialStateProperty.all(Colors.grey),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0)
                      ))
                  ),
                ),
              )
            ],
          ),
        )
    );
  }



}