import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../models/Person.dart';

class PhotoOpenPage extends StatelessWidget {
  final String _image;
  const PhotoOpenPage({required String image, Key? key})
      : _image = image,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Center(
            child: Image(
              image: ResizeImage(
                AssetImage(_image),
                width: MediaQuery.of(context).size.width.toInt()
              ),
            )
          ),
          /*SizedBox(
            width: double.infinity,
            height: MediaQuery.of(context).size.height / 3,
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.black.withOpacity(1.0), Colors.grey.withOpacity(0.0)],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter
                  )
              ),
              alignment: Alignment.topCenter,
              child: Padding(
                padding: EdgeInsets.only(left: 16.0, right: 14.0, top: 46.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 28,
                          height: 28,
                          child: CircleAvatar(
                            backgroundImage: AssetImage(photoToPersonMap[_image]!.avatar),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              photoToPersonMap[_image]!.name,
                              style: GoogleFonts.roboto(
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                                color: Colors.white
                              ),
                            ),
                            Text(
                              photoToPersonMap[_image]!.nickname,
                              style: GoogleFonts.roboto(
                                fontSize: 11,
                                fontWeight: FontWeight.w400,
                                color: Colors.white.withOpacity(0.8),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: Icon(Icons.close_outlined),
                      color: Colors.white,
                    )
                  ],
                ),
              ),
            ),
          ),*/
        ],
      ),
    );
  }
}
