import 'package:cached_network_image/cached_network_image.dart';
import 'package:figma_prototype/bloc/posts/posts_block.dart';
import 'package:figma_prototype/bloc/posts/posts_state.dart';
import 'package:figma_prototype/repositories/posts_repository/firebase_posts_repository.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../bloc/posts/posts_event.dart';
import '../../../models/Person.dart';
import '../../bottom_navigation_bar.dart';
import '../../../models/Tab.dart';
import '../../../models/post/post.dart';
import '../../components/title_text.dart';
import '../../photo_open_page.dart';


class DiscoverPage extends StatelessWidget {
  DiscoverPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => PostsBloc(
          postsRepository: FirebasePostsRepository()
        )..add(LoadPosts()),
        child: DiscoverView()
      );
  }
}

class DiscoverView extends StatelessWidget {
  DiscoverView({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //color: Colors.white,
      body: Padding(
        padding:
        const EdgeInsets.only(top: 32.0, bottom: 32.0, left: 16.0, right: 16.0),
        child: CustomScrollView(
          controller: ScrollController(),
          slivers: [
            const SliverToBoxAdapter(
              child: SizedBox(height: 32.0),
            ),
            SliverToBoxAdapter(
              child: Text(
                'Discover',
                style: GoogleFonts.comfortaa(
                  color: Colors.black,
                  fontSize: 36,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: -1.5,
                ),
              ),
            ),
            const SliverToBoxAdapter(
              child: SizedBox(height: 32.0),
            ),
            const SliverToBoxAdapter(
                child: TitleText('What\'s new today')
            ),
            const SliverToBoxAdapter(
              child: SizedBox(height: 24.0),
            ),

            SliverToBoxAdapter(
              child: BlocBuilder<PostsBloc, PostsState> (
                bloc: BlocProvider.of(context),
                  builder: (context, state) {
                    if(state is PostsLoading){
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    if(state is PostsLoadSuccess) {
                      return SizedBox(
                        width: double.infinity,
                        height: 388,
                        child: _ImageSlider(
                          posts: state.posts,
                          key: key,
                        ),
                      );
                    }
                    return Text('Something went wrong...');
                  }
              ),
            ),
            const SliverToBoxAdapter(
              child: SizedBox(height: 48.0),
            ),
            const SliverToBoxAdapter(
              child: TitleText(
                  'Browse all'
              ),
            ),
            const SliverToBoxAdapter(
              child: SizedBox(height: 24.0),
            ),
            /*SliverToBoxAdapter(
                child: MasonryGridView.count(
                  controller: ScrollController(),
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: photoToPersonMap.keys.toList().length,
                  itemBuilder: ((context, index) {
                    return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  fullscreenDialog: true,
                                  builder: (BuildContext context) =>
                                      PhotoOpenPage(
                                          image: photoToPersonMap.keys
                                              .toList()[index])));
                        },
                        child: Image(
                          image: ResizeImage(
                            AssetImage(photoToPersonMap.keys.toList()[index]),
                            width: 300,
                          ),
                        )
                    );
                  }),
                ),
              ),*/
            const SliverToBoxAdapter(
              child: SizedBox(height: 32.0),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: 52,
                child: TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.transparent),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        const RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(4.0)),
                            side: BorderSide(color: Colors.black, width: 2.0))),
                  ),
                  onPressed: () {},
                  child: Text(
                    'See more'.toUpperCase(),
                    style: GoogleFonts.roboto(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 13,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: AppBottomNavigationBar(
        currentTab: TabItem.DISCOVER, onSelectTab: (TabItem value) {  },
      ),
    );
  }
}

class _ImageSlider extends StatefulWidget {
  final List<Post> _posts;
  int _index = 0;

  _ImageSlider({required List<Post> posts, Key? key})
      : _posts = posts,
        super(key: key);

  @override
  State<StatefulWidget> createState() => _ImageSliderState();
}

class _ImageSliderState extends State<_ImageSlider> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: PageView.builder(
        itemCount: widget._posts.length,
        controller: PageController(
            viewportFraction: MediaQuery.of(context).size.width < 850
                ? 1.0
                : MediaQuery.of(context).size.width < 1100
                    ? 0.7
                    : 0.2),
        onPageChanged: (int index) => setState(() => widget._index = index),
        itemBuilder: (_, i) {
          return Transform.scale(
              scale: 1.0, child: _buildImage(i, double.infinity, 343.0));
        },
      ),
    );
  }

  Widget _buildImage(int index, double width, double height) {
    return FutureBuilder(
      future: getDownloadUrl(widget._posts[index].image),
      builder: ((BuildContext context, AsyncSnapshot<String> snapshot){
        if(snapshot.connectionState == ConnectionState.waiting) {
          return Container(
            width: width,
            height: height,
            color: Colors.grey,
          );
        }
        if(snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else {
          return CachedNetworkImage(imageUrl: snapshot.data!);
        }

      })
    );
    /*InkWell(
      onTap: () {
        /*Navigator.push(
            context,
            MaterialPageRoute(
                fullscreenDialog: true,
                builder: (BuildContext context) =>
                    PhotoOpenPage(image: widget._posts[index])));*/
      },
      child: Column(
        children: [
          Expanded(
            child: CachedNetworkImage(imageUrl: widget._posts[index].image.getDownloadURL(), height: height),
          ),
          /*const SizedBox(
            height: 16.0,
          ),
          Row(
            children: [
              SizedBox(
                width: 28,
                height: 28,
                child: CircleAvatar(
                  backgroundImage: AssetImage(
                      photoToPersonMap[widget._images[index]]!.avatar),
                ),
              ),
              SizedBox(
                width: 8,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    photoToPersonMap[widget._images[index]]!.name,
                    style: GoogleFonts.roboto(
                      fontSize: 13,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Text(
                    photoToPersonMap[widget._images[index]]!.nickname,
                    style: GoogleFonts.roboto(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                      color: Colors.black.withOpacity(0.8),
                    ),
                  )
                ],
              )
            ],
          )*/
        ],
      ),
    );*/
  }

  Future<String> getDownloadUrl(String imagePath) async{
    return await FirebaseStorage.instance.refFromURL(imagePath).getDownloadURL();
  }
}
