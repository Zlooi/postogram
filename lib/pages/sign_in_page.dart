import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import '../bloc/auth/auth_bloc.dart';
import 'home/discover/discover_page.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is Authenticated) {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => DiscoverPage())
            );
          }
          else if(state is AuthError) {
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text(state.error))
            );
          }
        },
        child: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            return Padding(
                padding: EdgeInsets.all(16.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      SizedBox(height: 16.0),
                      Align(
                          alignment: Alignment.topLeft,
                          child: SizedBox(
                            width: 48.0,
                            height: 48.0,
                            child: InkWell(
                              child: Icon(Icons.arrow_back),
                              onTap: () {
                                Navigator.pop(context);
                              },
                              radius: 32.0,
                              borderRadius: BorderRadius.circular(32.0),
                            ),
                          )
                      ),
                      Text(
                        'Log in',
                        style: GoogleFonts.comfortaa(
                          color: Colors.black,
                          fontStyle: FontStyle.normal,
                          fontSize: 36,
                          letterSpacing: -1.5,
                        ),
                      ),
                      SizedBox(height: 32.0),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'E-mail',
                          errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        validator: (value) {
                          return value != null &&
                              !EmailValidator.validate(value)
                              ? 'Enter a valid email'
                              : null;
                        },
                        controller: _emailController,
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'Password',
                          errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          focusedErrorBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(width: 3.0),
                              borderRadius: BorderRadius.zero
                          ),
                        ),
                        keyboardType: TextInputType.visiblePassword,
                        obscureText: true,
                        controller: _passwordController,
                      ),
                      const SizedBox(height: 16.0),
                      SizedBox(
                        height: 52,
                        child: TextButton(
                          onPressed: () {
                            _authenticateWithEmailAndPassword(
                                context);
                          },
                          child: Text(
                            'LOG IN',
                            style: GoogleFonts.roboto(
                                color: Colors.white
                            ),
                          ),
                          style: ButtonStyle(
                              elevation: MaterialStateProperty.all(1.0),
                              backgroundColor: MaterialStateProperty.all(Colors.black),
                              shadowColor: MaterialStateProperty.all(Colors.grey),
                              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0)
                              ))
                          ),
                        ),
                      ),
                      Spacer(),
                      if(state is Loading)
                        Center(
                          child: CircularProgressIndicator(),
                        )
                    ],
                  ),
                )
            );
          },
        )
      )
    );
  }

  void _authenticateWithEmailAndPassword(context) {
    if (_formKey.currentState!.validate()) {
      BlocProvider.of<AuthBloc>(context).add(
        SignInRequested(_emailController.text, _passwordController.text),
      );
    }
  }



}