

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../models/Tab.dart';

Map<TabItem, BottomTab> tabs = {
  TabItem.DISCOVER  : BottomTab(name: 'Discover', selectedColor: Colors.lightBlue, icon: CupertinoIcons.home),
  TabItem.SEARCH    : BottomTab(name: 'Search', selectedColor: Colors.lightBlue, icon: CupertinoIcons.search),
  TabItem.ADD       : BottomTab(name: 'Add', selectedColor: Colors.lightBlue, icon: CupertinoIcons.add, isMainButton: true),
  TabItem.MESSAGES  : BottomTab(name: 'Messages', selectedColor: Colors.lightBlue, icon: CupertinoIcons.chat_bubble),
  TabItem.PROFILE   : BottomTab(name: 'Profile', selectedColor: Colors.lightBlue, icon: CupertinoIcons.person),
};

class AppBottomNavigationBar extends StatelessWidget {
  final TabItem currentTab;
  final ValueChanged<TabItem> onSelectTab;

  AppBottomNavigationBar({required this.currentTab, required this.onSelectTab, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      selectedItemColor: _colorTabMatching(currentTab),
      selectedFontSize: 13,
      unselectedItemColor: Colors.grey,
      type: BottomNavigationBarType.fixed,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      currentIndex: currentTab.index,
      items: tabs.keys.map((e) => _buildItem(e)).toList(),
      onTap: (index) => onSelectTab(
          TabItem.values[index]
      ),
    );
  }

  BottomNavigationBarItem _buildItem(TabItem item){
    return BottomNavigationBarItem(
      icon: Icon(
        _iconTabMatching(item),
        color: _colorTabMatching(item),
      ),
      label: tabs[item]!.name,
      tooltip: tabs[item]!.name,
    );
  }

  IconData _iconTabMatching(TabItem item) => tabs[item]!.icon;

  Color _colorTabMatching(TabItem item) => currentTab == item? tabs[item]!.selectedColor : Colors.grey;


}