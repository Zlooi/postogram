import 'package:figma_prototype/pages/register_page.dart';
import 'package:figma_prototype/pages/sign_in_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoggedOutPage extends StatelessWidget {
  const LoggedOutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  fit: StackFit.expand,
                  alignment: Alignment.bottomCenter,
                  children: [
                    Image.asset(
                      'assets/logged_out_background.jpg',
                      fit: BoxFit.cover,
                    ),
                    Positioned.fill(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Postogram',
                          style: GoogleFonts.pacifico(
                              fontWeight: FontWeight.w700,
                              fontSize: 48,
                              color: Colors.white
                          ),
                        ),
                      ),
                    )
                  ],
                )
            ),
          ),
          Padding(
              padding: EdgeInsets.all(16.0),
              child: Container(
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    SizedBox(
                      width: 167,
                      height: 52,
                      child: TextButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                  side: BorderSide(color: Colors.black, width: 2.0)
                              )
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) => LoginPage()
                              )
                          );
                        },
                        child: Text(
                          'LOG IN',
                          style: GoogleFonts.roboto(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 13,
                          ),
                        ),
                      ),
                    ),
                    ConstrainedBox(constraints: BoxConstraints(maxWidth: 32),
                    ),

                    SizedBox(
                      width: 167,
                      height: 52,
                      child: TextButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Colors.black),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(4.0))
                              )
                          ),
                          elevation: MaterialStateProperty.all(1),
                          shadowColor: MaterialStateProperty.all(Colors.grey),
                        ),
                        child: Text(
                            'REGISTER',
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.w900,
                              fontSize: 13,
                            )
                        ),
                        onPressed: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => RegisterPage()
                            )
                          );
                        },
                      ),
                    ),

                  ],
                ),
              )
          )
        ],
      ),
    );
  }
}
