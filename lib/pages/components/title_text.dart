

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TitleText extends StatelessWidget {
  final String _title;

  const TitleText(String title, {Key? key}) : _title = title, super(key: key);



  @override
  Widget build(BuildContext context) {
    return Text(
      _title.toUpperCase(),
      style: GoogleFonts.roboto(
        color: Colors.black,
        fontSize: 13,
        fontWeight: FontWeight.w900,
        fontStyle: FontStyle.normal,
        letterSpacing: 0.5,
      ),
    );
  }

}