import 'dart:ui';

import 'bloc/auth/auth_bloc.dart';
import 'bloc/posts/posts_block.dart';
import 'pages/logged_out_page.dart';
import 'pages/home/discover/discover_page.dart';
import 'repositories/auth_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'firebase_options.dart';

Future<void> main() async {
  return BlocOverrides.runZoned(
      () async {
        WidgetsFlutterBinding.ensureInitialized();
        await Firebase.initializeApp(
          options: DefaultFirebaseOptions.currentPlatform,
        );
        final authenticationRepository = AuthRepository();
        //await authenticationRepository.user.first;
        //final postsRepository = FirebasePostsRepository();
        runApp(MyApp(authenticationRepository: authenticationRepository));
      },
  );
}

class MyApp extends StatelessWidget {
  final AuthRepository _authenticationRepository;

  const MyApp({Key? key,
    required AuthRepository authenticationRepository}) :
      _authenticationRepository = authenticationRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthBloc(
          authRepository: _authenticationRepository
      ),
      child: MaterialApp(
        home: StreamBuilder<User?>(
              stream: FirebaseAuth.instance.authStateChanges(),
              builder: (context, snapshot){
                if(snapshot.hasData){
                  return BlocProvider(
                    create: (context) => PostsBloc(
                      postsRepository: RepositoryProvider.of(context),
                    ),
                    child: DiscoverPage(key: key),
                  );
                }
                return LoggedOutPage(key: key);
              }
          ),
        ),
    );
  }
}

class AppView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
  
}


class AppScrollBehavior extends MaterialScrollBehavior  {
  @override
  Set<PointerDeviceKind> get dragDevices => {
    PointerDeviceKind.touch,
    PointerDeviceKind.mouse,
  };
}

