

import 'package:equatable/equatable.dart';

import '../../models/post/post.dart';

abstract class PostsEvent extends Equatable {
  const PostsEvent();

  @override
  List<Object?> get props => [];
}

class LoadPosts extends PostsEvent { }

class UpdatePosts extends PostsEvent {
  final List<Post> posts;


  UpdatePosts(this.posts) {}

  @override
  List<Object> get props => [posts];
}



