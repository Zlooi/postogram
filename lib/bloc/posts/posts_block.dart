import 'dart:async';

import 'package:figma_prototype/bloc/posts/posts_event.dart';

import '../../models/post/post.dart';
import 'posts_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../repositories/posts_repository/posts_repository.dart';

/// Posts BLoC provider.
/// Used for posts fetching state management
class PostsBloc extends Bloc<PostsEvent, PostsState> {
  final PostsRepository postsRepository;
  StreamSubscription? _databaseSubscription;

  PostsBloc({required this.postsRepository}) : super(PostsLoading()) {

    on<LoadPosts>((event, emit) async {
      emit(PostsLoading());
      try{
        await _databaseSubscription?.cancel();
        await emit.forEach<List<Post>>(postsRepository.getPosts(), onData: (posts) {
          return PostsLoadSuccess(posts: posts);
        });
      } catch (e){
        emit(PostsLoadFailure(e.toString()));
      }
    });

    on<UpdatePosts>((event, emit) async {
      emit(PostsLoadSuccess(posts: event.posts));
    });

  }
}