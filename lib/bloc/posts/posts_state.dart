import 'package:equatable/equatable.dart';
import 'package:figma_prototype/models/post/post.dart';
import 'package:flutter/foundation.dart';

@immutable
abstract class PostsState extends Equatable {}

// trying to load posts
class PostsLoading extends PostsState {
  @override
  List<Object?> get props => [];
}

// success loading
class PostsLoadSuccess extends PostsState {
  final List<Post> posts;

  PostsLoadSuccess({this.posts = const []});
  @override
  List<Object?> get props => [posts];
}

// loading error
class PostsLoadFailure extends PostsState {
  final String error;

  PostsLoadFailure(this.error);
  @override
  List<Object?> get props => [error];
}

