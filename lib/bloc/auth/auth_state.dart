part of 'auth_bloc.dart';


@immutable
abstract class AuthState extends Equatable {}

// initial state
class UnAuthenticated extends AuthState {
  @override
  List<Object?> get props => [];
}

// trying to authenticate
class Loading extends AuthState {
  @override
  List<Object?> get props => [];
}

// success authentication
class Authenticated extends AuthState {
  @override
  List<Object?> get props => [];
}

// authentication error
class AuthError extends AuthState {
  final String error;

  AuthError(this.error);
  @override
  List<Object?> get props => [error];
}

