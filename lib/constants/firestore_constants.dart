class FirestoreConstants {
  static const posts = 'posts';
  static const users = 'users';
  static const photoUrl = 'photoUrl';
  static const content = 'content';
  static const type = 'type';
}