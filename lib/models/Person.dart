import 'dart:collection';

import 'package:equatable/equatable.dart';

class Person extends Equatable {
  final String id;
  final String? email;
  final String? name;
  final String? avatar;
  final String? nickname;
  late final List<String> photos;

  Person({
    required this.id,
    this.email,
    this.name,
    this.avatar,
    this.nickname,
    List<String>? photos}) {
    this.photos = photos ?? [];
  }

  static final empty = Person(id: '');

  bool get isEmpty => this == Person.empty;

  @override
  List<Object?> get props => [email, id, name, avatar, nickname, photos];
}

class PersonsList {
  final List<Person> persons = [];
  final Map<String, Person> nicknameToPersonMap = {};

  PersonsList();

  Person getPersonByNickName(String nickname) {
    if(nicknameToPersonMap.containsKey(nickname)){
      return nicknameToPersonMap[nickname]!;
    } else {
      throw Exception('Person ${nickname} not found');
    }
  }
}

