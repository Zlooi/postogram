
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

import 'post_entity.dart';
import 'package:flutter/foundation.dart';

@immutable
class Post {
  final String id;
  final String author;
  final String image;
  final Timestamp date;


  const Post(this.author, {required this.id, required this.image, required this.date});

  Post copyWith({bool? complete, String? author, String? id, String? image, Timestamp? date}) {
    return Post(
      author ?? this.author,
      id: id ?? this.id,
      date: date ?? this.date,
      image: image ?? this.image,
    );
  }

  @override
  int get hashCode =>
      author.hashCode ^ id.hashCode ^ date.hashCode ^ image.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Post &&
              runtimeType == other.runtimeType &&
              author == other.author &&
              image == other.image &&
              id == other.id &&
              date == other.date;

  @override
  String toString() {
    return 'Post { id: $id, author: $author, date: $date, image: $image }';
  }

  PostEntity toEntity() {
    return PostEntity(id, author, date, image);
  }

  static Post fromEntity(PostEntity entity) {
    return Post(
      entity.author,
      image: entity.image,
      id: entity.id,
      date: entity.date,
    );
  }
}
