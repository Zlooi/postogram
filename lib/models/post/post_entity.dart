

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_storage/firebase_storage.dart';

class PostEntity extends Equatable {
  final String id;
  final String author;
  final Timestamp date;
  final String image;

  const PostEntity(this.id, this.author, this.date, this.image);

  factory PostEntity.fromJson(Map<String, Object> json) {
    return PostEntity(
      json['id'] as String,
      json['author'] as String,
      json['date'] as Timestamp,
      json['image'] as String
    );
  }

  factory PostEntity.fromSnapshot(DocumentSnapshot snap) {
    Map<String, dynamic> jsonMap = snap.data() as Map<String, dynamic>;
    return PostEntity(
        snap.id,
        jsonMap['author'],
        jsonMap['date'],
        jsonMap['image']
    );
  }

  Map<String, Object> toJson(){
    return {
      'id' : id,
      'author' : author,
      'date' : date,
      'image' : image
    };
  }

  Map<String, Object> toDocument(){
    return {
      'id' : id,
      'author' : author,
      'date' : date,
      'image' : image
    };
  }

  @override
  String toString() {
    return 'Post { id: $id, author: $author, date: $date, image: $image }';
  }



  @override
  List<Object?> get props => [];

}