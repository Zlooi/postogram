import 'package:flutter/material.dart';

class BottomTab {
  final String name;
  final MaterialColor selectedColor;
  final IconData icon;
  final bool isMainButton;

  BottomTab({
    required this.name,
    required this.selectedColor,
    required this.icon,
    isMainButton
  }) :
      isMainButton = isMainButton ?? false;
}

enum TabItem{
  DISCOVER, SEARCH, ADD, MESSAGES, PROFILE
}