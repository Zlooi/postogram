abstract class PersonRepository {
  Future<bool> isAuthenticated();

  Future<void> authenticate({required String email, required String password});

  Future<String> getUserId();
}
