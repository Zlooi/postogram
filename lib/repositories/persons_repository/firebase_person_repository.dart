import 'package:figma_prototype/models/Person.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'persons_repository.dart';

class FirebaseUserRepository implements PersonRepository {
  final FirebaseAuth _firebaseAuth;

  FirebaseUserRepository({FirebaseAuth? firebaseAuth})
      : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance;

  @override
  Future<bool> isAuthenticated() async {
    final currentUser = _firebaseAuth.currentUser;
    return currentUser != null;
  }

  @override
  Future<void> authenticate({required String email, required String password}) {
    return _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
  }

  Future<String> getUserId() async {
    return (_firebaseAuth.currentUser)?.uid ?? '';
  }
}