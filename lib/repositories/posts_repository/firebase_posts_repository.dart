
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:figma_prototype/models/post/post.dart';
import 'package:figma_prototype/models/post/post_entity.dart';

import 'posts_repository.dart';

class FirebasePostsRepository extends PostsRepository{
  final postsCollection = FirebaseFirestore.instance.collection('posts');

  @override
  Future<void> addNewPost(Post post) {
    return postsCollection.add(post.toEntity().toDocument());
  }

  @override
  Future<void> deletePost(Post post) {
    return postsCollection.doc(post.id).delete();
  }

  @override
  Stream<List<Post>> getPosts() {
    return postsCollection.snapshots().map((snapshot) {
      return snapshot.docs.map((doc) => Post.fromEntity(PostEntity.fromSnapshot(doc))).toList();
    });

  }

  @override
  Future<void> updatePost(Post post) {
    return postsCollection.add(post.toEntity().toDocument());
  }
  
}