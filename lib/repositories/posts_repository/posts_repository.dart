

import '../../models/post/post.dart';

abstract class PostsRepository {
  Future<void> addNewPost(Post post);
  Future<void> deletePost(Post post);
  Stream<List<Post>> getPosts();
  Future<void> updatePost(Post post);
}