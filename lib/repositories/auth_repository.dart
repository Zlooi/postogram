

import 'package:firebase_auth/firebase_auth.dart';

class AuthRepository {
  final _firebaseAuth = FirebaseAuth.instance;

  Future<void> register({required String email, required String password}) async {
    try{
      await FirebaseAuth.instance.createUserWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      if(e.code == 'weak-password') {
        throw Exception('The provided password is too weak.');
      } else if (e.code == 'email-already-in-use') {
        throw Exception('An account for that email already exists.');
      }
    } catch(e) {
      rethrow;
    }
  }

  Future<void> signIn({required String email, required String password}) async {
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: password);
    } on FirebaseAuthException catch (e) {
      switch(e.code) {
        case('user-not-found'):
          throw Exception('No user found for this email.');
        case('wrong-password'):
          throw Exception('Wrong password');
      }
    } catch(e) {
      rethrow;
    }
  }

  Future<void> signOut() async {
    try {
      await _firebaseAuth.signOut();
    } catch (e) {
      throw Exception(e);
    }
  }

}